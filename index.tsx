import { h, mount, patch } from 'petit-dom';

window.addEventListener('load', _ => {
  let appNode = <div/>;
  document.body.appendChild(mount(appNode));

  let firstName = 'Tomas';
  let lastName = 'Hubelbauer';
  let isActive = true;

  function onFirstNameInputInput(event: Event) {
    firstName = (event.currentTarget as HTMLInputElement).value;
    render();
  }

  function onLastNameInputInput(event: Event) {
    lastName = (event.currentTarget as HTMLInputElement).value;
    render();
  }

  function onIsActiveInputChange(event: Event) {
    isActive = (event.currentTarget as HTMLInputElement).checked;
    render();
  }

  function render() {
    const tempNode = appNode;
    appNode = <div>
      <div>
        <input value={firstName} oninput={onFirstNameInputInput} />
        <input value={lastName} oninput={onLastNameInputInput} />
        <input type="checkbox" checked={isActive} onchange={onIsActiveInputChange} />
      </div>
      <div>{firstName} {lastName} {isActive ? '' : '(inactive)'}</div>
    </div>;
    patch(appNode, tempNode);
  }

  render();
});
