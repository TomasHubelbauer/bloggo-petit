# Petit

Trying out Petit with TypeScript and Parcel.
Inspired by [this benchmark](https://github.com/krausest/js-framework-benchmark).

Thanks to using TypeScript and Parcel, we get JSX/TSX without hassle.

## Running

`parcel index.html`

## Integrating

`index.tsx`:

```typescript
import { h, mount, patch } from 'petit-dom';

let app = <div />;
window.addEventListener('load', async () => {
    document.body.appendChild(mount(app));
    await render();
});

async function render() {
    const temp = app;
    app =
        <div>
            {await …}
        </div>;
    patch(app, temp);
}
```

`tsconfig.json`:

```json
{
    "compilerOptions": {
        "jsx": "react",
        "jsxFactory": "h"
    }
}
```
